import { NavLink } from "react-router-dom";
import { Link, Button } from "@material-ui/core"
import styles from "./NavBar.module.css"

export default function NavBar(){
    return(
        <nav className={styles.wrapper}>
            <Link component={NavLink} to="/">
                <Button color="primary">
                    Home
                </Button>
            </Link>
            <Link component={NavLink} to="/formulario">
                <Button color="primary">
                    Formulario
                </Button>
            </Link>
        </nav>
    )
}