import { Paper, Typography } from "@material-ui/core"
import styles from "./Post.module.css"

export default function Post({userId, body, title}){
    return(
        <Paper className={styles.wrapper}>
            <Typography variant="caption" color="primary" gutterBottom>
                {title}
            </Typography>
        </Paper>
    )
}