import { Typography } from "@material-ui/core"
import { useQuery } from "react-query"
import Post from "../Post"
import fetch from "../../utils/fetch"

export default function Home(){
    const { data, status, error } = useQuery("posts", () => fetch("https://jsonplaceholder.typicode.com/posts"))

    if(status === "loading"){
        return <p>Loading...</p>
    }

    if(status === "error"){
        return <p>Error</p>
    }

    console.log(data)

    return(
        <div>
            <Typography variant="h2">
                Posts
            </Typography>
            {
                data.map((post) => <Post key={post.id} {...post} />)
            }            
        </div>
    )
}