import {Fragment} from "react"
import { Switch, Route } from "react-router-dom"
import NavBar from "./components/NavBar"
import Home from "./components/Home"
import Detalle from "./components/Detalle"
import Formulario from "./components/Formulario"
import "./App.css"

function App() {
  return (
    <Fragment>
      <NavBar/>
      <div className="main">
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/detalle" component={Detalle}/>
          <Route exact path="/formulario" component={Formulario}/>
        </Switch>
      </div>
    </Fragment>
  );
}

export default App;
